Module: Mobile Gesture Engagement Pack

Author: errnio <http://errnio.com>


-- SUMMARY --

*The Mobile Gesture pack adds a new engagement and revenue layer to a mobile version of any drupal site with a simple addition of a module. 
The module enables several gesture based tools and page functions to be added to any mobile browsing session of the site.

*Top bar
*Bottom Bar
*Text Selection features
*Slide Motion Features (in development)
*Zoom and Tilt Features (in development)
*The tools act as enhancements to regular browser elements, which enhance the experience of the user on a mobile device with functions of search,
 text selection, and page options like share and more.

*For a full description and bug reports of the module, search the module project pages for errnio products on drupal.org


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see our tutorial instructions on the module page or in this PDF link:
http://errnio.com/drupal/errnio-drupal-instructions.pdf


-- CONFIGURATION --

* Configure a unique errnio ID for submitting a request at http://errnio.com and pasting the ID received into the configuration page on the errnio module configuration in your admin.


-- CUSTOMIZATION --

* only available upon direct request at http://errnio.com
configurations are possible for the different functions and features stated above. 


-- FAQ --

Q What is errnio?
A errnio adds gesture based engagement and monetization to the mobile view of your site, and enhances user engagement with additional mobile functionality. errnio allows you to enjoy added revenue for your mobile traffic while any other monetization units continue to work undisturbed (no banners or ads).

Q What is the difference between errnio and other monetization units?
A Glad you asked, other then the fact that errnio is a new type of monetization relying on the user's finger movements across your website, it also contains NO banners or popups! Most monetization products for mobile compete with each other for the same space they take on you site. errnio can live side by side with any banner (or display/ad unit) and you can profit from two revenue streams rather then only one.

Q Does errnio run on desktop screens?
A Niet!

Q After I download the plugin, what do I do to get my money?
A After you installed the plugin you should get a notification requesting your site-id. Go to our site, errnio.com and register. Once you've done this, you'll get a site-id which you can paste into the options page of the plugin (follow the notification). After everything is ready you can manage your plugin on the errnio dashboard and collect your revenues.

Q What are the rates? How much money can I make?
A We have a simple revenue share program which you can access once registered. You can contact us for more information sales@errnio.com

Q Can errnio harm my site code or functionality?
A Absolutely NO! errnio is fully encapsulated from your code(or WP code) and can in no way impact anything in your site. Nevertheless if you do experience something that bothers you errnio will be glad to assist at support@errnio.com


-- CONTACT --

Current maintainers:
* errnio team - http://www.drupal.org/user/3189381
mail: info@errnio.com
